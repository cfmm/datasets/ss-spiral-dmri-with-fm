function [List_lambdas] = getLambdas(Wavelet,varargin)
    %%% Wavelet is a wavelet object that cointains all wavelet coefficients
    %%% and we need to identify lambda around high frequency wavelet
    %%% coefficients.
    
    %%% HowToGetLambda is a flag to identify the method of choice:
    % 0. Assumes the distribution as Rayleigh and get the std from it
    % 1. Use k-means to identify between noise-, mixed- ,and signal-based
    % coefficients
    
    if isempty(varargin)
        %%% use as default the one that appears to perform the best...
        HowToGetLambda = 0;
    else
        HowToGetLambda = varargin{1};
    end
    
    L = size(Wavelet.high,2);
    List_lambdas = zeros(L,1);
    
    switch(HowToGetLambda)
        case 0
            for i = 1 : L
                h = histogram(gather(abs(Wavelet.high{i}(:))),round(numel(Wavelet.high{i}(:))/100));
                vals = h.Values;
                vals(1) = 0; % The first bin can be inflated if there was masking in im0
                [~,threshInd] = max(vals);
                lamVal = 0.5*(h.BinEdges(threshInd) + h.BinEdges(threshInd+1))/sqrt(2);
                %%% Tweeking a little bit lamVal to avoid
                %%% over-regularization
                lamVal = lamVal/2;
                
                
                List_lambdas(i) = lamVal;
            end
        case 1
            for i = 1 : L
            % NEW
%             figure,
%             vals = gather(abs(Wavelet.high{i}(:)));
%             h = histogram(vals,256);
%             vals = h.Values(2:end);
%             [~,pos] = max(vals);
%             kgroups = 2;
%             map_of_groups = kmeans(vals',kgroups); % noise-, mixed- and signal-based wavelet coefficients as clusters.
%             bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
%             first_threshold = h.BinEdges(bin_pos);
%             
%             vals = gather(abs(Wavelet.high{i}(:)));
%             vals(vals > first_threshold) = [];
%             
%             h = histogram(vals,64);
%             vals = h.Values(2:end);
%             [~,pos] = max(vals);
%             kgroups = 2;
%             map_of_groups = kmeans(vals',kgroups); % noise-, mixed- and signal-based wavelet coefficients as clusters.
%             bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
%             
%             if (bin_pos == length(vals)) || (bin_pos == 1)
%                 %bin_pos = round(length(vals)/2) + 1;
%                 threshold = first_threshold/(sqrt(2));
%             else
%                 bin_pos = bin_pos+1;
%                 threshold = h.BinEdges(bin_pos)/(sqrt(2));
%             end
%             List_lambdas(i) = threshold;
%             close
            % NEW
            % Selected
                figure,
                vals = gather(abs(Wavelet.high{i}(:)));
                h = histogram(vals,256);
                first_threshold = h.BinEdges(2);
                vals(vals > first_threshold) = [];
                
                h = histogram(vals,64);
                vals = h.Values(2:end);
                [~,pos] = max(vals);
                kgroups = 2;
                map_of_groups = kmeans(vals',kgroups); % noise-, mixed- and signal-based wavelet coefficients as clusters.
                bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
                if (bin_pos == length(vals)) || (bin_pos == 1)
                    bin_pos = round(length(vals)/2) + 1;
                else
                    bin_pos = bin_pos+1;
                end
                threshold = h.BinEdges(bin_pos); % +1 to compensate for the removed value
                List_lambdas(i) = min([threshold, first_threshold]) / sqrt(2);
                %[threshold, first_threshold]
                close
            % Selected
            % OLD
%                 h = histogram(gather(abs(Wavelet.high{i}(:))),round(numel(Wavelet.high{i}(:))/100));
%                 vals = h.Values;
%                 %
%                 vals(1) = []; % we remove the count from 0-valued coefficients 
%                               % to make easier the splitting b
%                 [~,pos] = max(vals);
%                 kgroups = 2;
%                 map_of_groups = kmeans(vals',kgroups); % noise-, mixed- and signal-based wavelet coefficients as clusters.
%                 bin_pos = find(map_of_groups == map_of_groups(pos),1,'last');
% 
%                 % We divide by sqrt(2) b/c we're dealing with magnitude
%                 % while the bfista algorithm is using complex values for softthresholding.
% 
%                 List_lambdas(i) = h.BinEdges(bin_pos) / sqrt(2); % assuming a conservative value for the threshold
%                                                            % think of this as floor() instead of the average
%                                                            % between edges.
               % OLD
            end        
    end

end