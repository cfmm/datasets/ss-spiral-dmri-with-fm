%% Single-Shot diffusion-weighted imaging at 7T using the expanded encoding model with compressed sensing
%%% Gabriel Varela-Mattatall*, Paul I. Dubovan*, Tales Santini, Kyle Gilbert, Ravi S. Menon, Corey A. Baron
%%% MRM, 2023
%%% Link: https://arxiv.org/abs/2211.07532 
%% Summary of the Paper (Long Story Short)
%%% We include compressed sensing regularization with the expanded encoding model
%%% to compensate for SNR reduction when
%%% increasing the acceleration factor and/or when increasing the spatial resolution.
%%% We also include a method to determine the regularization
%%% weighting from one of our previous work (same idea but from a different perspective...) 
%%% for tuning-free compressed sensing reconstruction.
%% Demo
%%% This demo includes the data to obtain the reconstructions used to generate
%%% Figure 3 (Fractional Anisotropy Maps) and Supporting Information Figure S5 (Color Fractional Anisotropy Maps)
%%% from the manuscript previously mentioned. Later, use MRTrix3 to generate FA maps (fa.nii) and color FA maps (ev.nii).
%% Additional Relevant References to cite when using this demo:
%[1] Dubovan PI, Baron CA. Model‐based determination of the synchronization delay between MRI and trajectory data. 
%Magn Reson Med. 2023;89(2):721-728. doi:10.1002/mrm.29460

%[2] Varela‐Mattatall G, Baron CA, Menon RS. Automatic determination of the regularization weighting for wavelet‐based compressed sensing MRI reconstructions. 
%Magnet Reson Med. 2021;86(3):1403-1419. doi:10.1002/mrm.28812

%%
% clear all; close all; clc;
%% User
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Please only manipulate the variables in this block until you feel
% confident to play around with the code. 

% Example: '/Users/Documents/MATLAB/matmri-master'
directory   = '/'; % where the matMRI Toolbox is located
docase      = 2;        % Expected a recontruction time of XXX min depending on your system
saverecon   = 0;        % Save reconstructions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Include dependencies 
addpath(directory);
run setPath
%% Reconstruction Setting
% docase is an ID to run each specific case.
% We provide 4 cases based on 2 subjects and 2 specific acquisitions

% additional data could be uploaded upon request.

% Retrieve data
switch docase 
    case 1
        code = 'v1_1p5mm_af4'; % approx 12 min
        load('Volunteer1data_1p5mm_usf4.mat');
    case 2
        code = 'v2_1p5mm_af4'; 
        load('Volunteer2data_1p5mm_usf4.mat');
    case 3
        code = 'v1_1p1mm_af5';  % approx 24-55 min
        load('Volunteer1data_1p1mm_usf5.mat');
    case 4
        code = 'v2_1p1mm_af5'; % 
        load('Volunteer2data_1p1mm_usf5.mat');
end


% Some general parameters
% Related to Optimization
opt.interpThresh = 0;
maxIt_LS = 20;
opt.stopOnResInc = 0;
opt.resThresh = 1e-6;
opt.imNFull = szIm; 
maxIt_CS = 1000;

ls = zeros(szIm);
cs = zeros(szIm);
bValues = length(data);
% reconstruction process along slices
for n = 1:bValues
    Crcvr_n = Crcvr{n,1};
    data_n = double(data{n,1});
    datatime_n = datatime{n,1};
    kspha_n = double(kspha{n,1});
    kcoco_n = double(kcoco{n,1});
    
    R = rcvrOp(Crcvr_n,0);
    S = sampHighOrder(b0map, datatime_n(:), kspha_n', kcoco_n', phs_sph_grid, [], [], [], opt.interpThresh>0, opt.interpThresh);
    optSampFunc.daNFull = [length(datatime_n),1,size(Crcvr_n,4)];
    optSampFunc.imNFull = size(phs_sph_grid.x);
    opFunc = @(x,transp) mrSampFunc(x,transp,S,R,optSampFunc);
    tic
    ls_temp = cgne(opFunc,data_n(:),[],maxIt_LS,opt); 
    ls(:,:,n) = reshape(ls_temp,szIm);
    ls_time = toc
    
    levelsPerDim = [1, 1];
    isDec = 0; % Non-decimated to avoid blocky edges
    W = dwt(levelsPerDim,szIm,isDec);

    wim = W*ls(:,:,n);

    lambdaMethod = 0;
    lamVal = getLambdas(wim,lambdaMethod);

    lambdaCS = waveletObj(lamVal(1),wim); % This replicates the scalar lamVal into all wavelet fields using the template wim_zf
    lambdaCS = fillHighFreqWithLambdas(lamVal,lambdaCS);
    lambdaCS.low = 0;

    % Do a few cgne iterations to get a better starting guess
    x0 = cgne(opFunc,data_n(:),[],2); 
    x0 = reshape(x0,szIm); 
    tic
    cs_temp = bfista(opFunc,data_n(:),W,lambdaCS,x0(:),maxIt_CS,opt);
    cs(:,:,n) = reshape(cs_temp,szIm);
    cs_time = toc
end

showAllbs = @(data) abs(reshape(data(:,:,2:end),[szIm(1),szIm(2)*(bValues-1)]))
figure, imshow([showAllbs(ls);showAllbs(cs)],[])

if saverecon
    save(code,'cs','ls');
end

disp('DONE :)');
