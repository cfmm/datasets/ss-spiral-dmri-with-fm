function [Wavelet_Obj] = fillHighFreqWithLambdas(List_Lambdas , Wavelet_Obj)

    n = length(List_Lambdas);
    
    for i = 1 : n
        Wavelet_Obj.high{i} = {List_Lambdas(i),List_Lambdas(i),List_Lambdas(i)};
    end

end