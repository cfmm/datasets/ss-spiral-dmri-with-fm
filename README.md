# Single-Shot Spiral diffusion-weighted imaging at 7T using the expanded encoding model with compressed sensing
 Gabriel Varela-Mattatall*, Paul I. Dubovan*, Tales Santini, Kyle Gilbert, Ravi S. Menon, Corey A. Baron

 Official Link to publication: ...SOON.

 Arxiv Link: 

## Summary
This work investigated the combination of the expanded encoding model with compressed sensing regularization. The expanded encoding model characterizes phase pertubations during the acquisition whilst compressed sensing compensates for SNR reduction when pursuing higher spatial resolutions or higher acceleration factors. Overall, both short readout times and high spatial resolutions are desirable to avoid T2* blurring and partial volume effects, respectively. However, these come at the expense of reduced SNR, and this is why including compressed sensing becomes relevant. Finally, this is the data repository to reproduce the reconstructions necessary to generate Figure 3 and Supporting Information Figure S5 from our manuscript.

## Getting started

Please be sure to have:
   
1- Matlab. 

2- MatMRI Toolbox (v0.1.07 https://zenodo.org/record/7620697#.Y-QKky_72Ls).
NOTE: MORE SPECIFIC DETAILS BY FOLLOWING THE LINK.

3- MrTrix3 (https://www.mrtrix.org). 

## Please cite the following publications depending on the topic.
MatMRI Toolbox:

[0] Single-Shot Spiral diffusion-weighted imaging at 7T using the expanded encoding model with compressed sensing
 Gabriel Varela-Mattatall*, Paul I. Dubovan*, Tales Santini, Kyle Gilbert, Ravi S. Menon, Corey A. Baron  

Alignment between MRI and Field Monitoring data:

[1] Dubovan PI, Baron CA. Model‐based determination of the synchronization delay between MRI and trajectory data. 
Magn Reson Med. 2023;89(2):721-728. doi:10.1002/mrm.29460

Regulization Weighting for bFISTA:

[2] Varela‐Mattatall G, Baron CA, Menon RS. Automatic determination of the regularization weighting for wavelet‐based compressed sensing MRI reconstructions. 
Magnet Reson Med. 2021;86(3):1403-1419. doi:10.1002/mrm.28812

## Authors and acknowledgment
Authors wish to acknowledge funding from CIHR grant FRN 148453, the NSERC Discovery Grant [RGPIN-2018-05448], Canada Research Chairs [950-231993], Ontario Research Fund [37907], BrainsCAN-the Canada First Research Excellence Fund award to Western University, and the NSERC PGS D program. Finally, authors want to thank Mr. Trevor Szekeres and Mr. Scott Charlton for the data acquisition. 

## Project status
CLOSED - This work was used to generate matmri v0.1.07
